<div class="control-group">
    <input type="text" name="contact.name" placeholder=" Nome"
           class="form-control" value="${contact.name}" />
</div>
<div class="control-group" style="margin-top: 30px;">
    <input type="text" name="contact.nickname" placeholder=" Apelido"
           class="form-control" value="${contact.nickname}" />
</div>
<div class="control-group" style="margin-top: 30px;">
    <input type="text" name="contact.address" placeholder=" Endere�o"
           class="form-control" value="${contact.address}" />
</div>
<h3 class="h3 text-left" style="margin-top: 50px;">Contato:</h3>
<hr>
<div class="control-group" style="margin-top: 30px;">
    <input type="text" name="contact.cellPhoneNumber"
           placeholder=" Celular" class="form-control"
           value="${contact.cellPhoneNumber}" />
</div>
<div class="control-group" style="margin-top: 30px;">
    <input type="text" name="contact.phoneNumber" placeholder=" Telefone"
           class="form-control" value="${contact.phoneNumber}" />
</div>
<div class="control-group" style="margin-top: 30px;">
    <input type="email" name="contact.email" placeholder=" Email"
           class="form-control" value="${contact.email}" />
</div>
<h3 class="h3 text-left" style="margin-top: 50px;">Social:</h3>
<hr>
<div class="control-group" style="margin-top: 30px;">
    <input type="text" name="contact.facebook" placeholder=" Facebook"
           class="form-control" value="${contact.facebook}" />
</div>
<div class="control-group input-group" style="margin-top: 30px;">
    <span class="input-group-addon" id="basic-addon1">@</span> <input
        type="text" name="contact.twitter" placeholder=" Twitter"
        class="form-control" aria-describedby="basic-addon1"
        value="${contact.twitter}" />
</div>

<div class="control-group" style="margin-top: 30px;">
    <button type="submit" class="btn btn-primary">
        Salvar &nbsp; <span class="glyphicon glyphicon-ok"></span>
    </button>
</div>