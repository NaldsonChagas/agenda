<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../_comum/header.jsp"></c:import>
    <div class="container col-md-5 content">
        <h2 class="h2 text-center">Altere o contato: ${contact.name}</h2>

    <form style="margin-top: 40px; margin-bottom: 40px;"
          action="${linkTo[ContactController].change()}" method="post">
        <input type="hidden" value="${contact.id}" name="contact.id" />
        <c:import url="../_comum/form.jsp"></c:import>
    </form>
</div>
<c:import url="../_comum/footer.jsp"></c:import>