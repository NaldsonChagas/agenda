package br.com.naldson.agenda.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import br.com.naldson.agenda.model.Contact;

public class ContactDao {

    private EntityManager em;

    @Inject
    public ContactDao(EntityManager em) {
        this.em = em;
    }

    @Deprecated
    public ContactDao() {
    }

    public List<Contact> list() {
        TypedQuery<Contact> query = em.createQuery("SELECT c FROM Contact c", Contact.class);
        return query.getResultList();
    }

    public void save(Contact contact) {
        em.getTransaction().begin();
        em.persist(contact);
        em.getTransaction().commit();
    }

    public void change(Contact c) {
        em.getTransaction().begin();
        Contact contact = search(c.getId());
        setAttributes(contact, c);
        em.persist(contact);
        em.getTransaction().commit();
    }

    private void setAttributes(Contact contact, Contact c) {
        contact.setAddress(c.getAddress());
        contact.setCellPhoneNumber(c.getCellPhoneNumber());
        contact.setEmail(c.getEmail());
        contact.setFacebook(c.getFacebook());
        contact.setName(c.getName());
        contact.setNickname(c.getNickname());
        contact.setPhoneNumber(c.getPhoneNumber());
        contact.setTwitter(c.getTwitter());
    }

    public Contact search(int id) {
        Contact contact = em.find(Contact.class, id);
        return contact;
    }

    public void remove(int id) {
        em.getTransaction().begin();
        em.remove(search(id));
        em.getTransaction().commit();
    }
}