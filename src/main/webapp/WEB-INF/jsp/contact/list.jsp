<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:import url="../_comum/header.jsp"></c:import>
    <div class="container col-md-9 content">
        <h2 class="h2 text-center">Todos os usu�rios cadastrados</h2>
        <br>
        <table class="table table-striped table-responsive">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Apelido</th>
                    <th>Endere�o</th>
                    <th>Celular</th>
                    <th>Telefone</th>
                    <th>Email</th>
                    <th>Twitter</th>
                    <th>Facebook</th>
                </tr>
            </thead>
            <tbody>
            <c:forEach items="${contacts}" var="contact">
                <tr>
                    <td>${contact.id}</td>
                    <td>${contact.name}</td>
                    <td>${contact.nickname}</td>
                    <td>${contact.address}</td>
                    <td>${contact.cellPhoneNumber}</td>
                    <td>${contact.phoneNumber}</td>
                    <td>${contact.email}</td>
                    <td>@${contact.twitter}</td>
                    <td>${contact.facebook}</td>


                    <td>
                        <form action="${linkTo[ContactController].formChange()}" method="post">
                            <input type="hidden" name="id" value="${contact.id}">
                            <button type="submit" class="btn btn-primary">
                                Alterar &nbsp; <span class="glyphicon glyphicon-cog"></span>
                            </button>
                        </form>
                    </td>
                    <td>
                        <form action="${linkTo[ContactController].remove()}" method="post">
                            <input type="hidden" name="id" value="${contact.id}">
                            <button
                                onclick="return confirm('Deseja mesmo remover este contato?')"
                                type="submit" class="btn btn-danger">
                                Remover &nbsp; <span class="glyphicon glyphicon-trash"></span>
                            </button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>
<c:import url="../_comum/footer.jsp"></c:import>