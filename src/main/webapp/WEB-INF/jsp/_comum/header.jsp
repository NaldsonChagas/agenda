<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css"
              href="<c:url value='/bootstrap/css/bootstrap.min.css'/>" />
        <link rel="stylesheet" type="text/css"
              href="<c:url value='/css/style.css'/>" />
        <title>Agenda</title>
    </head>
    <body>
        <script type="text/javascript" src="<c:url value='/js/jquery.min.js'/>"></script>
        <script type="text/javascript"
        src="<c:url value='/bootstrap/js/bootstrap.min.js'/>"></script>
        <header>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                                aria-controls="navbar">
                            <span class="sr-only"></span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span> <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="${linkTo[IndexController].index()}">Agenda</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="${linkTo[IndexController].index()}">Home <span
                                        class="glyphicon glyphicon-home"></span></a></li>
                            <li><a href="${linkTo[ContactController].list()}">Contatos
                                    <span class="glyphicon glyphicon-user"></span>
                                </a></li>
                            <li><a href="${linkTo[ContactController].formNew()}">Novo
                                    contato <span class="glyphicon glyphicon-plus"></span>
                                </a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>