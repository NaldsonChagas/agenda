package br.com.naldson.agenda.controller;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.interceptor.IncludeParameters;
import br.com.caelum.vraptor.validator.Validator;
import br.com.naldson.agenda.dao.ContactDao;
import br.com.naldson.agenda.model.Contact;

@Controller
public class ContactController {

    private ContactDao contactDao;
    private Validator validator;
    private Result result;

    @Inject
    public ContactController(ContactDao contactDao, Validator validator, Result result) {
        this.contactDao = contactDao;
        this.validator = validator;
        this.result = result;
    }

    @Deprecated
    public ContactController() {

    }

    @Path("contatos/novo")
    public void formNew() {

    }

    @Path("contatos/alterar")
    public void formChange(int id) {
        Contact contact = contactDao.search(id);
        result.include("contact", contact);
    }

    @Path("contatos")
    public void list() {
        List<Contact> contacts = contactDao.list();
        result.include("contacts", contacts);
    }

    @IncludeParameters
    public void save(@Valid Contact contact) {
        validator.onErrorRedirectTo(this).formNew();
        contactDao.save(contact);
        result.redirectTo(this).list();
    }

    @IncludeParameters
    public void change(@Valid Contact contact) {
        validator.onErrorRedirectTo(this).formNew();
        contactDao.change(contact);
        result.redirectTo(this).list();
    }

    public void remove(int id) {
        System.out.println(id);
        contactDao.remove(id);
        result.redirectTo(this).list();
    }
}
